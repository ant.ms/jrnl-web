<?php
    $selectedTheme = "teal lighten-3 #80cbc4";
    if (isset($_COOKIE['selectedTheme']))
        $selectedTheme = substr($_COOKIE['selectedTheme'], 0, strrpos($_COOKIE['selectedTheme'], " "));
    $selectedHex = strrchr($_COOKIE['selectedTheme'], " ");
?>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="style.css">
        <link rel="manifest" href="manifest.webmanifest">

        <title>jrnl - Web</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="theme-color" content="#4285f4">
        <link rel="shortcut icon" href="favicon.png" type="image/png">
    </head>

    <?php
        // check if necesairy files exist, otherwise redirect to install page
        if (!file_exists('data/posts.sqlite') || !file_exists('data/config.php'))
            header("Location: install.php");

        include 'data/config.php';

        // set login if it needs setting
        // (this page only receives a post request, when it's tasked with logging the user in)
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            setcookie("email", $_POST['email'], time() + 86400, ".");
            setcookie("password", $_POST['password'], time() + 86400, ".");
            
            header('Location: .');
        } else {
            if (isset($_GET['logout'])) {
                setcookie("email", "", time() - 3600);
                setcookie("password", "", time() - 3600);
                header('Location: .');
            }
        }
        // echo hash("sha512", "admin" . "admin");

        // open database
        $db = new SQLite3('data/posts.sqlite', SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);

        $dateOfPosts = (isset($_GET['date'])) ? $_GET['date'] : date("Y-m-d");
        if ($dateOfPosts == "")
            $dateOfPosts = date("Y-m-d");
    ?>

    <body>

        <!-- Dropdown Structure Add -->
        <ul id="addDropdown" class="dropdown-content">
            <li><a data-target="add" class="modal-trigger"><i class="material-icons">edit</i> Add Post</a></li>
            <li><a data-target="addspecial" class="modal-trigger"><i class="material-icons">today</i> Add Special</a></li>
        </ul>

        <nav class="<?php echo $selectedTheme ?>">
            <div class="nav-wrapper">
                <ul class="left">
                    <?php
                        if($_COOKIE['alwaysOnSidebar'] == 'false')
                            echo "<li><a href='#' data-target='slide-out' class='sidenav-trigger'><i class='material-icons'>menu</i></a></li>";
                    ?>
                    <!-- region navigation-button -->
                        <?php
                            $statement = $db->prepare("SELECT dateofcreation FROM posts ORDER BY dateofcreation ASC");
                            $result = $statement->execute();
                            
                            $datesToCheck = array();

                            while($row = $result->fetchArray(SQLITE3_ASSOC))
                                array_push($datesToCheck, $row["dateofcreation"]);

                            for ($i=0; $i < sizeof($datesToCheck); $i++)
                                if (strtotime(substr($datesToCheck[$i], 0, 11)) > strtotime($dateOfPosts)) {
                                    echo "<li><a href='?date=" . substr($datesToCheck[$i], 0, 11) . "'><i class='material-icons'>chevron_left</i></a></li>";
                                    break;
                                }
                        ?>
                        <?php
                            $statement = $db->prepare("SELECT dateofcreation FROM posts ORDER BY dateofcreation DESC");
                            $result = $statement->execute();
                            
                            $datesToCheck = array();

                            while($row = $result->fetchArray(SQLITE3_ASSOC))
                                array_push($datesToCheck, $row["dateofcreation"]);

                            for ($i=0; $i < sizeof($datesToCheck); $i++)
                                if (strtotime(substr($datesToCheck[$i], 0, 11)) < strtotime($dateOfPosts)) {
                                    echo "<li><a href='?date=" . substr($datesToCheck[$i], 0, 11) . "'><i class='material-icons'>chevron_right</i></a></li>";
                                    break;
                                }
                        ?>
                    <!-- endregion -->
                </ul>
                <a href="index.php" class="brand-logo hide-on-med-and-down" style="margin-left: 15px">jrnlWeb</a>
                
                <div style="position:absolute;left:50%;display:block;transform: translate(-50%);" class="hide-on-med-and-down">
                    <span id="onThisDay"> <!-- TO BE FILLED IN BY JS --> </span>
                </div>

                <ul class="right">
                    <li <?php if($_COOKIE['alwaysOnSidebar'] == 'false') echo "class='hide-on-small-only'"; ?>><a href="?logout"><i class="material-icons">logout</i></a></li>
                    <li <?php if($_COOKIE['alwaysOnSidebar'] == 'false') echo "class='hide-on-small-only'"; ?>><a href="output.php"><i class="material-icons">file_download</i></a></li>
                    <li <?php if($_COOKIE['alwaysOnSidebar'] == 'false') echo "class='hide-on-small-only'"; ?>><a data-target="settings" class="modal-trigger"><i class="material-icons">settings</i></a></li>
                    <li class="hide-on-small-only"><a onclick="darkModeSwitch()"><i class="material-icons">brightness_medium</i></a></li>
                    
                    <!-- <li>
                        <form onclick="switchSearch()">
                            <div class="input-field" style="width: 3.5rem" id="searchBar">
                                <input id="search" type="search" required>
                                <label class="label-icon" for="search" onclick="switchSearch()">
                                    <i class="material-icons" style="color:white" id="searchIcon">search</i>
                                </label>
                                <i class="material-icons" id="searchClose" style="visibility:hidden">close</i>
                            </div>
                        </form>
                    </li> -->

                    <input type="text" class="datepicker" id="date" style="visibility:hidden;position:absolute">
                    <li><a onclick="M.Datepicker.getInstance(document.getElementById('date')).open()"><i class="material-icons">today</i></a></li>
                    <li><a class="dropdown-trigger" href="#!" data-target="addDropdown"><i class="material-icons">add</i></a></li>
                </ul>
                <div style="clear:both"></div>
            </div>
        </nav>

        <!-- region login-protection -->
            <?php
                // kick user out if not logged in
                $password = hash("sha512", $_COOKIE["email"] . $_COOKIE["password"]);
                if ($password != $passwdHash1 && $password != $passwdHash2) {
                    echo "<div class='row'><div class='container' style='margin-top: 30px'><div class='row'><h1>Please Login</h1></div><div class='row'><form class='col s12' action='.' method='POST'>
                        <div class='row'><div class='input-field col s12'>
                            <input id='email' name='email' type='text' class='validate'>
                            <label for='email'>Username</label></div>
                        </div><div class='row'><div class='input-field col s12'>
                            <input id='password' name='password' type='password' class='validate'>
                            <label for='password'>Password</label></div>
                        </div><div class='row'>
                            <button class='btn waves-effect waves-light $selectedTheme' type='submit' name='action' style='float:right'>Login
                            <i class='material-icons right'>send</i>
                            </button></div>
                    </form></div></div><script src='https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js'></script></body></html>";
                    exit();
                }
            ?>
        <!-- endregion -->


    <?php
        if ($password == $passwdHash1)
            $currentUser = 1;
        else if ($password == $passwdHash2)
            $currentUser = 2;
    ?>

        <!-- region special content -->

            <?php
                $specialDays = array();

                $statement = $db->prepare("SELECT date FROM specialdays");
                $result = $statement->execute();
                
                while($row = $result->fetchArray(SQLITE3_ASSOC) ) {
                    // $row = str_replace("\n", "<br>", $row);
                    array_push($specialDays, $row['date']);
                }
            ?>

        <!-- endregion special content -->

        <!-- region desktop sidenav -->
                <?php
                    if ($_COOKIE['alwaysOnSidebar'] == 'false') {
                        echo "
                        <ul id='slide-out' class='sidenav'>
                            <li>
                                <div class='user-view hide-on-small-only'>
                                    <div class='background'></div><br>
                                </div>
                                <div class='hide-on-med-and-up <?php echo $selectedTheme ?>'>
                                    <ul>
                                        <div class='row'>
                                            <div class='col s4'>
                                                <li><a data-target='settings' class='modal-trigger'><i class='material-icons white-text'>settings</i></a></li>
                                            </div>
                                            <div class='col s4'>
                                                <li><a href='output.php'><i class='material-icons white-text'>file_download</i></a></li>
                                            </div>
                                            <div class='col s4'>
                                                <li><a href='?logout'><i class='material-icons white-text'>logout</i></a></li>
                                            </div>
                                        </div>
                                    </ul>
                                </div>
                            </li>";

                        // $statement = $db->prepare("SELECT * FROM 'posts'");
                        $statement = $db->prepare("SELECT strftime('%Y-%m-%d', dateofcreation) AS dates FROM posts GROUP BY dates ORDER BY dates DESC");
                        $result = $statement->execute();
                        
                        while($row = $result->fetchArray(SQLITE3_ASSOC) ){
                            $element = $row['dates'];
                            $thisDate = date_create($element);
                            $humanDate = date_format($thisDate, "jS F Y");
                            $active = ($dateOfPosts == $element) ? " active" : "";
                            $special = (in_array($element, $specialDays)) ? " :star:" : "";
                            echo "<li class='is-a-date'><a name='$element' href='?date=$element' class='notyetemoji is-a-date collection-item$active'>$humanDate$special</a></li>";
                        }
                    }

                    echo "</ul>";
                ?>
        <!-- endregion -->

        <!-- region tablet sidebar -->
            <?php
                if ($_COOKIE['alwaysOnSidebar'] == 'true') {
                    echo "<div class='row' style='margin-bottom:0'><div class='col' style='box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.2), 0 0 20px 0 rgba(0, 0, 0, 0.19);width: 230px;max-height: calc(100vh - 64px);
                    overflow-y: scroll;position: fixed;top: 64px;'><ul class='collection' style='border: unset;'>";

                    // $statement = $db->prepare("SELECT * FROM 'posts'");
                    $statement = $db->prepare("SELECT strftime('%Y-%m-%d', dateofcreation) AS dates FROM posts GROUP BY dates ORDER BY dates DESC");
                    $result = $statement->execute();
                    
                    while($row = $result->fetchArray(SQLITE3_ASSOC) ){
                        $element = $row['dates'];
                        $thisDate = date_create($element);
                        $humanDate = date_format($thisDate, "jS F Y");
                        $active = ($dateOfPosts == $element) ? " active" : "";
                        $special = (in_array($element, $specialDays)) ? " :star:" : "";
                        echo "<li class='is-a-date'><a name='$element' href='?date=$element' class='notyetemoji is-a-date collection-item$active'>$humanDate$special</a></li>";
                    }

                    echo "</ul></div><div class='col s12' style='float:none; width: calc(100vw - 230px)'>";
                }
            ?>
        <!-- endregion tablet sidebar -->

        <!-- region content -->
            <?php
                if (in_array($dateOfPosts, $specialDays)) {
                    $statement = $db->prepare("SELECT * FROM specialdays WHERE date = '$dateOfPosts'");
                    $result = $statement->execute();
                    
                    while($row = $result->fetchArray(SQLITE3_ASSOC) ) {
                        // $row = str_replace("\n", "<br>", $row);
                        // echo $row['title'];
                        $headerimageurl = $row['headerimageurl'];
                        $imageurl = $row['imageurl'];
                        $title = $row['title'];

                        if($_COOKIE['parallaxToggle'] != 'false') {
                            echo "<div class='parallax-container'><div class='parallax'><img src='$headerimageurl'>
                                    <h1 style='position: absolute;text-align: center;width: 100%;margin-top: 200px;color:white'>$title</h1>
                                    </div></div>";
                        } else {
                            echo "<style>
                            #cutiecontainer { margin-top: 135px }
                            @media (max-width: 550px) { #cutiecontainer { margin-top: 100px } #cutieImage { border-radius: 0 } }
                            </style>";
                        }
                        
                        echo "<div class='col s12' id='cutiecontainer'>";
                        echo "<div class='card horizontal $cardColor' style='margin-top:-100px;border-radius:12px'><div class='card-stacked'>
                        <div class='card-content' id='cutieCard'>
                        <img class='materialboxed' src='$imageurl' id='cutieImage'><div>";
                        if($_COOKIE['parallaxToggle'] == 'false')
                            echo "<h3 style='margin-top: -10px'>$title</h3>";
                        echo "<p class='notyetemoji'>";
                        echo str_replace("\n", "<br>", $row['content']); 
                        echo "</p></div></div></div></div></div>";
                    }
                    
                }
            ?>
            <div class="container">
            <div class="col" <?php if ($_COOKIE['alwaysOnSidebar'] == 'true') echo "style='max-height: calc(100vh - 64px);overflow-y: scroll;scrollbar-width: none;'" ?> id="mainContent">
            <div class="row">
                    <?php
                        if (!in_array($dateOfPosts, $specialDays))
                            echo "<h1>" . date_format(date_create("$dateOfPosts"), "jS F Y") . "</h1>";
                    ?>
                </div>
                <div class="row">
                    <script src="emoji.js"></script>

                    <?php
                        $noPost = true;

                        // $statement = $db->prepare("SELECT * FROM 'posts'");
                        $statement = $db->prepare("SELECT * FROM posts WHERE dateofcreation BETWEEN '$dateOfPosts 00:00:00' AND '$dateOfPosts 23:59:59'");
                        $result = $statement->execute();
                        
                        while($row = $result->fetchArray(SQLITE3_ASSOC) ) {
                            // get color
                            if ($row['createdby'] == 1)
                                $cardColor = $colorUser1;
                            else if ($row['createdby'] == 2)
                                $cardColor = $colorUser2;
                            else
                                $cardColor = "red";

                            $row = str_replace("\n", "<br>", $row);
                            echo "<div class='col s12'><div class='card horizontal $cardColor'><div class='card-stacked'><div class='card-content'><p class='notyetemoji'>";
                            if (isset($_COOKIE['titleDisplayType'])) {
                                if ($_COOKIE['titleDisplayType'] == "header")
                                    echo "<h5>" . $row['title'] . "</h5>";
                                else if ($_COOKIE['titleDisplayType'] == "underline")
                                    echo "<u>" . $row['title'] . "</u><br>";
                                else if ($_COOKIE['titleDisplayType'] == "normal")
                                    echo $row['title'] . "<br>";
                                else
                                    echo "<b>" . $row['title'] . "</b><br>";
                            }
                            echo $row['content']; 
                            // echo $row['dateofcreation'];
                            echo "</p></div></div></div></div>";

                            $noPost = false;
                        }

                        if ($noPost == true) {
                            echo "<div class='noPostIcon'><i class='material-icons'>wb_sunny</i><p>No posts here yet</p></span>";
                        }
                    ?>
                </div>
            </div>
                
            </div>
        <!-- endregion content -->

            </div>
        </div>

        <!-- region add modal -->
            <div id="add" class="modal">
                <div class="modal-content">
                    <h4>Add new Post</h4>
                    <div class="row">
                        <form class="col s12" action="add-post.php" method="POST" id="addForm">
                            <input type="hidden" name="date" value="<?php echo $dateOfPosts ?>">
                            <input type="hidden" name="user" value="<?php echo $currentUser ?>">
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">title</i>
                                    <input id="icon_telephone" type="text" name="title" class="validate" required>
                                    <label for="icon_telephone">Title</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">mode_edit</i>
                                    <textarea id="icon_prefix2" class="materialize-textarea" name="message" required></textarea>
                                    <label for="icon_prefix2">Message</label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancel</a>
                    <button class="btn waves-effect waves-light <?php echo $selectedTheme ?>" type="submit" name="action" onclick="document.getElementById('addForm').submit()">Submit<i class="material-icons right">send</i>
                    </button>
                </div>
            </div>
        <!-- endregion -->

        <!-- region add special modal -->
        <div id="addspecial" class="modal">
                <div class="modal-content">
                    <h4>Add new Special Day</h4>
                    <div class="row">
                        <form class="col s12" action="add-special.php" method="POST" id="addSpecial">
                            <input type="hidden" id="specialdate" name="specialdate" value="<?php echo $dateOfPosts ?>">
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">title</i>
                                    <input id="specialtitle" type="text" name="specialtitle" class="validate" required>
                                    <label for="specialtitle">Title</label>

                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s6">
                                    <i class="material-icons prefix">insert_link</i>
                                    <input id="specialimageurl" type="url" name="specialimageurl" class="validate" placeholder="https://cdn.ant.lgbt/cutie/..." required>
                                    <label for="specialimageurl">vertical Image URL</label>
                                </div>
                                <div class="input-field col s6">
                                    <i class="material-icons prefix">insert_link</i>
                                    <input id="specialheaderimageurl" type="url" name="specialheaderimageurl" class="validate" placeholder="https://cdn.ant.lgbt/cutie/..." required>
                                    <label for="specialheaderimageurl">header Image URL</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">mode_edit</i>
                                    <textarea id="specialcontent" class="materialize-textarea" name="specialcontent" required></textarea>
                                    <label for="specialcontent">Description</label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancel</a>
                    <button class="btn waves-effect waves-light <?php echo $selectedTheme ?>" type="submit" name="action" onclick="document.getElementById('addSpecial').submit()">Submit<i class="material-icons right">send</i>
                    </button>
                </div>
            </div>
        <!-- endregion -->


        <!-- region settings modal -->
            <div id="settings" class="modal">
                <div class="modal-content">
                    <h4>Settings</h4>
                    <div class="row">
                        <div class="row">
                            <h5>Design</h5>
                        </div>
                        <div class="row">
                            <div class="col s6">
                                <?php
                                    $selectedDisplayType = "bold";
                                    if (isset($_COOKIE['titleDisplayType']))
                                        $selectedDisplayType = $_COOKIE['titleDisplayType'];
                                ?>
                                <!-- region title formatting -->
                                    <div class="row">
                                        <div class="col l6 m12">
                                            <label>
                                                <input name="titleDisplayType" type="radio" value="header" <?php if($selectedDisplayType == "header") echo "checked"; ?> />
                                                <span style="color:black"><i class="material-icons">text_fields</i> Show titles as header</span>
                                            </label>
                                            <label>
                                                <input name="titleDisplayType" type="radio" value="underline" <?php if($selectedDisplayType == "underline") echo "checked"; ?> />
                                                <span style="color:black"><i class="material-icons">text_format</i> Underline titles</span>
                                            </label>
                                        </div>
                                        <div class="col l6 m12">
                                            <label>
                                                <input name="titleDisplayType" type="radio" value="bold" <?php if($selectedDisplayType == "bold") echo "checked"; ?> />
                                                <span style="color:black"><i class="material-icons">format_bold</i> Display as bold text</span>
                                            </label>
                                            <label>
                                                <input name="titleDisplayType" type="radio" value="normal" <?php if($selectedDisplayType == "normal") echo "checked"; ?> />
                                                <span style="color:black"><i class="material-icons">short_text</i> Display as normal text</span>
                                            </label>
                                        </div>
                                    </div>
                                <!-- endregion title formatting -->
                            </div>
                            <div class="col s6">
                                <div class="input-field col s12">
                                    <select id="newTheme">
                                        <option value="teal lighten-3 #80cbc4" data-icon="colors/teal.png" <?php if($selectedTheme == "teal lighten-3 #80cbc4") echo "selected"; ?> >Teal (default)</option>
                                        <option value="deep-purple lighten-2 #9575cd" data-icon="colors/purple.png" <?php if($selectedTheme == "deep-purple lighten-2 #9575cd") echo "selected"; ?> >Purple</option>
                                        <option value="light-blue lighten-3 #81d4fa" data-icon="colors/light-blue.png" <?php if($selectedTheme == "light-blue lighten-3 #81d4fa") echo "selected"; ?> >Light Blue</option>
                                        <option value="green lighten-2 #81c784" data-icon="colors/green.png" <?php if($selectedTheme == "green lighten-2 #81c784") echo "selected"; ?> >Green</option>
                                    </select>
                                    <label>Materialize Select</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="switch">
                                    <label>
                                        Toggleable sidenav
                                        <input type="checkbox" id="hideSidenavToggle" <?php if($_COOKIE['alwaysOnSidebar'] == 'true') echo "checked" ?> >
                                        <span class="lever"></span>
                                        Always shown sidenav
                                    </label>
                                </div>
                            </div>
                            <div class="col">
                                <div class="switch">
                                    <label>
                                        Title
                                        <input type="checkbox" id="parallaxToggle" <?php if($_COOKIE['parallaxToggle'] == 'true') echo "checked" ?> >
                                        <span class="lever"></span>
                                        Parallax
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancel</a>
                    <button class="btn waves-effect waves-light <?php echo $selectedTheme ?>" name="action"  onclick="checkSettings()">Save<i class="material-icons right" onclick="checkSettings()">save</i>
                    </button>
                </div>
            </div>
        <!-- endregion -->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script src="script.js"></script>
    </body>
    <style>
        .collection .collection-item.active {
            background-color: <?php echo $selectedHex ?>;
        }
    </style>
    <script>
        function setPickerColor() {
            // set colors
            document.getElementsByClassName('datepicker-date-display')[0].classList.add(("<?php echo $selectedTheme ?>").split(" ")[0]);
            document.getElementsByClassName('datepicker-date-display')[0].classList.add(("<?php echo $selectedTheme ?>").split(" ")[1]);

            // make colors dark if necesairy
            if (localStorage.getItem('mode') == 'dark')
                document.getElementsByClassName('datepicker-date-display')[0].classList = document.getElementsByClassName('datepicker-date-display')[0].classList.toString().replace("lighten", "darken");
        }
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <?php
        $db->close();
    ?>
</html>