<?php
    $debugging = false;

    // open database
    $db = new SQLite3('data/posts.sqlite', SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);

    $title = strip_tags($_POST['title']);
    $content = strip_tags($_POST['message']);

    #region Markdown

    if ($debugging) echo "Input: <br>" . $content . "<br><br>";

    $lines = explode("\n", $content);

    foreach ($lines as $key => $line) {
        if ($debugging) echo $key . ". Input: " . $lines[$key]. "<br>";

        // italic and bold
        $lines[$key] = preg_replace('/\*\*\*(.*?)\*\*\*/', '<b><i>$1</i></b>', $lines[$key]);
        $lines[$key] = preg_replace('/___(.*?)___/', '<b><i>$1</i></b>', $lines[$key]);
        
        // bold
        $lines[$key] = preg_replace('/\*\*(.*?)\*\*/', '<b>$1</b>', $lines[$key]);
        $lines[$key] = preg_replace('/__(.*?)__/', '<b>$1</b>', $lines[$key]);
        
        // italic
        $lines[$key] = preg_replace('/\*(.*?)\*/', '<i>$1</i>', $lines[$key]);
        $lines[$key] = preg_replace('/_(.*?)/', '<i>$1</i>', $lines[$key]);
        
        // strikethrough
        $lines[$key] = preg_replace('/\~\~(.*?)\~\~/', '<s>$1</s>', $lines[$key]);

        // special characters
        $lines[$key] = str_replace("(c)", "&copy;", $lines[$key]);
        $lines[$key] = str_replace("(C)", "&copy;", $lines[$key]);
        $lines[$key] = str_replace("(r)", "&reg;", $lines[$key]);
        $lines[$key] = str_replace("(R)", "&reg;", $lines[$key]);
        $lines[$key] = str_replace("(tm)", "&trade;", $lines[$key]);
        $lines[$key] = str_replace("(TM)", "&trade;", $lines[$key]);
        $lines[$key] = str_replace("(p)", "&sect;", $lines[$key]);
        $lines[$key] = str_replace("(P)", "&sect;", $lines[$key]);
        $lines[$key] = str_replace("+-", "&plusmn;", $lines[$key]);

        // links
        $lines[$key] = preg_replace('/\[(.+)\]\((.+)\)/', '<a href=$2>$1</a>', $lines[$key]);
        
        if ($debugging) echo $key . ". Output:" . $lines[$key]. "<br><br>";
    }

    $content = implode("\n", $lines);

    #endregion

    if ($debugging) echo $content;
    if ($debugging) exit();

    $date = strip_tags($_POST['date']);
    $currentUser = strip_tags($_POST['user']);
    $db->query("INSERT INTO posts (title, content, createdby, dateofcreation) VALUES (\"$title\", \"$content\", $currentUser, \"$date 12:00\")");

    $db->close();

    header("Location: index.php?date=$date");
?>