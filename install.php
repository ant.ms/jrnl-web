<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <title>jrnl - Web</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="manifest.webmanifest">
    <link rel="shortcut icon" href="favicon.png" type="image/png">
</head>
<?php
        // set login if it needs setting
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            setcookie("email", $_POST['email'], time() + 86400, ".");
            setcookie("password", $_POST['password'], time() + 86400, ".");
            
            // create sqlite Database
            $db = new SQLite3('data/posts.sqlite', SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);

            #region create tables

            $db->query('CREATE TABLE IF NOT EXISTS "posts" (
                "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                "title" VARCHAR,
                "content" TEXT,
                "createdby" INTEGER,
                "dateofcreation" DATETIME DEFAULT CURRENT_TIMESTAMP
            )');

            $db->query('CREATE TABLE IF NOT EXISTS "specialdays" (
                "date" DATETIME PRIMARY KEY NOT NULL,
                "title" VARCHAR,
                "content" TEXT,
                "imageurl" VARCHAR,
                "headerimageurl" VARCHAR
            )');

            #endregion

            // create sample post (may be extended in the future)

            $db->query('INSERT INTO "posts" ("title", "content")
            VALUES ("Hello World", "This is the first posts")');

            // save login-data
            file_put_contents('data/config.php', '<?php $nameUser1 = "Main-User"; $colorUser1 = "light-green lighten-5"; $passwdHash1 =  \'' . hash("sha512", $_POST['email1'] . $_POST['password1']) . '\'; $nameUser1 = "Secondary-User"; $colorUser2 = "blue lighten-5"; $passwdHash2 = \'' . hash("sha512", $_POST['email2'] . $_POST['password2']) . '\'; ?>');

            // redirect to main page
            header('Location: .');
        }
?>

<body>
    <nav class="deep-purple lighten-2">
        <div class="nav-wrapper">
            <a href="index.php" class="brand-logo center" style="margin-left: 15px">jrnlWeb - Install</a>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <h1>Install Jrnl-Web</h1>
        </div>
        <div class="row">
            <div class="col s12">
                <ul class="collapsible">
                    <li>
                        <div class="collapsible-header">
                            <i class="material-icons">filter_drama</i>
                            First, check permissions
                            <?php
                                    echo (is_writable(dirname('data/test.txt'))) ? "<span class='badge'><i class='material-icons'>check</i></span>" : "<span class='badge'><i class='material-icons'>clear</i></span>" ;
                                ?>
                        </div>
                        <div class="collapsible-body">
                            <?php
                                    echo (is_writable(dirname('data/test.txt'))) ? "<span>Everything seems to be okay here</span>" : "<span>Make sure that the directory you specified during the <i>docker run</i> command is writable for this user.</span>" ;
                                ?>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header">
                            <i class="material-icons">filter_drama</i>
                            Second, check if files already existant
                            <?php
                                    echo (!file_exists('data/posts.sqlite') && !file_exists('data/config.php')) ? "<span class='badge'><i class='material-icons'>check</i></span>" : "<span class='badge'><i class='material-icons'>clear</i></span>";
                                ?>
                        </div>
                        <div class="collapsible-body">
                            <?php
                                    echo (!file_exists('data/posts.sqlite') && !file_exists('data/config.php')) ? "<span>Everything seems to be okay here</span>" : "<span>Either file <i>data/config.php</i> or file <i>posts.sqlite</i> still exist, please correct this and then reload the page.</span>" ;
                                ?>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <?php
                    $disabled = "";

                    if (file_exists('data/posts.sqlite') || file_exists('data/config.php'))
                        $disabled = "disabled";
                    if (!is_writable(dirname('data/test.txt')))
                        $disabled = "disabled";
                ?>

            <form action="install.php" method="POST">
                <div class="row">
                    <div class="input-field col s12 l6">
                        <input type="text" id="email1" name="email1" class="validate" required <?php echo $disabled ?>>
                        <label for="email">Username (User1)</label>
                    </div>
                    <div class="input-field col s12 l6">
                        <input type="password" id="password1" name="password1" class="validate" required
                            <?php echo $disabled ?>>
                        <label for="password">Password (User1)</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l6">
                        <input type="text" id="email2" name="email2" class="validate" required <?php echo $disabled ?>>
                        <label for="email">Username (User2)</label>
                    </div>
                    <div class="input-field col s12 l6">
                        <input type="password" id="password2" name="password2" class="validate" required
                            <?php echo $disabled ?>>
                        <label for="password">Password (User2)</label>
                    </div>
                </div>
                <div class="row">
                    <button class="btn waves-effect waves-light" type="submit" name="action" style="float:right"
                        <?php echo $disabled ?>>Install
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </form>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.collapsible');
            var instances = M.Collapsible.init(elems);
        });
    </script>
</body>

</html>