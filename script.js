// this function allows the software to "fix" itself, when it's first opened (in  theory)
if (localStorage.getItem("installed") != "true")
    checkSettings();

darkModeCheck();
getOnThisDay();

//#region onThisDay

async function getOnThisDay() {
    const urlParams = new URLSearchParams(window.location.search);
    let dateInUrl = new Date(urlParams.get('date'));
    
    if (dateInUrl.getTime() == 0)
        dateInUrl = new Date();
    
    var onThisDayRequest = new XMLHttpRequest();
    onThisDayRequest.onreadystatechange = function() {
        let response = JSON.parse(onThisDayRequest.responseText);
        let event = response.events[Math.floor(Math.random() * response.events.length)];
        onThisDay.innerText = `${event.description} (${event.year})`;
    }
    onThisDayRequest.open("GET", `https://byabbe.se/on-this-day/${dateInUrl.getMonth() + 1}/${dateInUrl.getDate() + 1}/events.json`, true); // true for asynchronous 
    onThisDayRequest.send(null);
    
    console.log(`https://byabbe.se/on-this-day/${dateInUrl.getMonth() + 1}/${dateInUrl.getDate() + 1}/events.json`);
}

//#endregion

//#region darkMode

function darkModeSwitch() {
    localStorage.setItem('mode', (localStorage.getItem('mode') || 'dark') === 'dark' ? 'light' : 'dark');
    darkModeCheck();
}

function darkModeCheck() {
    if (localStorage.getItem('mode') == 'dark') {
        document.querySelector('body').classList.add('dark');
        document.querySelector('nav').classList = document.querySelector('nav').classList.toString().replace("lighten", "darken");
     }
     else {
         document.querySelector('body').classList.remove('dark');
         document.querySelector('nav').classList = document.querySelector('nav').classList.toString().replace("darken", "lighten");
     }
}

//#endregion

function checkSettings() {
    localStorage.setItem("installed", "true");

    let needReload = true;

    // title formatting
    document.cookie = "titleDisplayType=" + document.querySelector('input[name="titleDisplayType"]:checked').value;

    // color theme
    document.cookie = "selectedTheme=" + newTheme.value;

    // sidenav
    document.cookie = "alwaysOnSidebar=" + hideSidenavToggle.checked;

    document.cookie = "parallaxToggle=" + parallaxToggle.checked;

    // change login
    // if (newPassword.value != "") {
    //     needReload = false;
    //     newLoginForm.submit();
    // }

    // reload if haven't already
    if (needReload == true)
        window.location.reload();
}


function switchSearch() {
    console.log("switch");
    if (searchClose.style.visibility == "visible") {
        searchBar.style.width = "3.5rem";
        searchClose.style.visibility = "hidden";
        searchIcon.style.color = "white";
    } else {
        searchBar.style.width = "15rem";
        searchClose.style.visibility = "visible";
        searchIcon.style.color = "black";
    }
}

//#region serviceWorker

if ('serviceWorker' in navigator) {
    console.log('CLIENT: service worker registration in progress.');
    navigator.serviceWorker.register('service-worker.js').then(function () {
        console.log('CLIENT: service worker registration complete.');
    }, function () {
        console.log('CLIENT: service worker registration failure.');
    });
} else {
    console.log('CLIENT: service worker is not supported.');
}

//#endregion

//#region Emoji

var emoji = new EmojiConvertor();
emoji.img_set = "twitter";
emoji.img_sets.twitter.path = "https://files.ant.lgbt/emoji-data/img-twitter-72/"
emoji.img_sets.twitter.sheet = "https://files.ant.lgbt/emoji-data/sheet_twitter_64.png";
document.querySelectorAll('.notyetemoji').forEach((element) => {
    element.innerHTML = emoji.replace_colons(element.innerHTML);
});

//#endregion

//#region dates

var dates = [];
document.querySelectorAll('.is-a-date').forEach((element) => {
    dates.push((new Date(element.name)).toDateString())
});

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.datepicker');
    let options = {
        firstDay: 1,
        maxDate: new Date(),
        format: "yyyy-mm-dd",
        onOpen() {
            setPickerColor();
        },
        onClose() {
            location.href = "?date=" + document.getElementById("date").value;
        },
        events: dates
    }
    M.Datepicker.init(elems, options);
});

//#endregion

//#region MaterializeCSS

document.addEventListener('DOMContentLoaded', function () {
    M.Sidenav.init(document.querySelectorAll('.sidenav'));
    M.Modal.init(document.querySelectorAll('.modal'));
    M.FormSelect.init(document.querySelectorAll('select'));
    M.Parallax.init(document.querySelectorAll('.parallax'));
    M.Materialbox.init(document.querySelectorAll('.materialboxed'));
    M.Dropdown.init(document.querySelectorAll('.dropdown-trigger'));
});

//#endregion