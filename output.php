<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="style.css">
        <title>jrnl - Web</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="manifest" href="manifest.webmanifest">
        <script>
            if (localStorage.getItem('darkmode') == 'true')
                document.head.insertAdjacentHTML("beforeend", `<link rel="stylesheet" href="dark.css">`);
        </script>
        <link rel="shortcut icon" href="logo.png" type="image/png">
        <style>
            @media print {
                .fixed-action-btn {display:none;}
            }
        </style>
        <link rel="shortcut icon" href="favicon.png" type="image/png">
    </head>
    <?php
        if (!file_exists('data/posts.sqlite') || !file_exists('data/config.php')) {
            header("Location: install.php");
        }
        include 'data/config.php';
    ?>

    <body>
        <?php
            // kick user out if not logged in
            $password = hash("sha512", $_COOKIE["email"] . $_COOKIE["password"]);
            if ($password != $passwdHash) {
                echo "<div class='row'><div class='container' style='margin-top: 30px'><div class='row'><h1>Please Login</h1></div><div class='row'><form class='col s12' action='.' method='POST'>
                    <div class='row'><div class='input-field col s12'>
                        <input id='email' name='email' type='text' class='validate'>
                        <label for='email'>Username</label></div>
                    </div><div class='row'><div class='input-field col s12'>
                        <input id='password' name='password' type='password' class='validate'>
                        <label for='password'>Password</label></div>
                    </div><div class='row'>
                        <button class='btn waves-effect waves-light' type='submit' name='action' style='float:right'>Login
                        <i class='material-icons right'>send</i>
                        </button></div>
                  </form></div></div><script src='https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js'></script></body></html>";
                exit();
            }
        ?>

        <div class="container">
            <script src="emoji.js"></script>
            <?php
                $db = new SQLite3('data/posts.sqlite', SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);
                $statement = $db->prepare("SELECT * FROM 'posts'");
                $result = $statement->execute();
                
                while($row = $result->fetchArray(SQLITE3_ASSOC) ) {
                    echo "<h4>" . date("jS F Y", strtotime($row['dateofcreation'])) . "</h4>";
                    $value = str_replace("\n", "<br>", $value);
                    echo "<div class='col s12'><div class='card horizontal'><div class='card-stacked'><div class='card-content'><p class='notyetemoji'>";
                    if (isset($_COOKIE['titleDisplayType'])) {
                        if ($_COOKIE['titleDisplayType'] == "header")
                            echo "<h5>" . $row['title'] . "</h5>";
                        else if ($_COOKIE['titleDisplayType'] == "underline")
                            echo "<u>" . $row['title'] . "</u><br>";
                        else if ($_COOKIE['titleDisplayType'] == "bold")
                            echo "<b>" . $row['title'] . "</b><br>";
                    }
                    echo $row['content']; 
                    echo "</p></div></div></div></div>";
                }
                $db->close();
            ?>
            <div class="fixed-action-btn <?php echo $selectedTheme ?>">
                <a class="btn-floating btn-large <?php echo $selectedTheme ?>" href=".">
                    <i class="large material-icons">arrow_back</i>
                </a>
            </div>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script src="script.js"></script>
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                var elems = document.querySelectorAll('.fixed-action-btn');
                var instances = M.FloatingActionButton.init(elems);
            });
        </script>
    </body>
</html>